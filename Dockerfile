FROM java:8
MAINTAINER Bernd Fischer "bfischer@mindapproach.de"
ENV MODIFIED_AT 2015-09-25_2055

# directory /opt is already present in image "java:8",
# is empty and owned by root
WORKDIR /opt
